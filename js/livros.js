var app = new Vue({
    el:'#app',
    data:{
       books:[],
       MySearch:''
    },
    methods:{

    },
    mounted: function() {
        var self = this;
        self.$http.get('data.json').then(function(response){
            console.log(response);
            self.books = response.data;
        });
    },
    computed: {
        list() {
            const filter = this.MySearch;      
            if (_.isEmpty(filter)) {
              return list;
            }
            //http://www.vuejs-brasil.com.br/vue-js-2-filtros-em-listas/
            return _.filter(list, repo => repo.name.indexOf(filter) >= 0);
        }
    },
    filters: {
        uppercase: function (value) {
            return value.toUpperCase();
        },
        currency: function (value) {
            return 'R$ ' + parseFloat(value).toFixed(2);
        }
    }
});