var hello = new Vue({
    el: '#hello',
    data: {
        msg: 'Hello Vue.js!',
        pessoas: [
            {name: 'Maria'},
            {name: 'Ana'},
            {name: 'Gabriela'},
            {name: 'Natália'},
            {name: 'Monique'},
        ],
        newElement: '',
        elements: [],
        objectA: {
            'font-size':'30px'
        },
        objectB: {
            'color':'red'
        },
        myListForm: [],
        myForm: {
            name: '',
            email: ''
        }
    },
    methods: {
        addElement: function(event) {
            //console.log(event);
            //event.preventDefault();

            var title = this.newElement.trim();

            if(title) {
                this.elements.push({title:title});
                this.newElement = '';
            }
            
        },
        removeElement: function(event, index) {
            event.preventDefault();
            this.elements.splice(index,1);
        },
        myClick: function(event) {
            alert('teste');
        },
        myKeyUp: function(event) {
            console.log('acertou mizeravi!');
        },
        addForm: function(event) {
            this.myListForm.push({
                name: this.myForm.name,
                email: this.myForm.email
            });

            this.myForm.name = '';
            this.myForm.email = '';


        }
    }
});